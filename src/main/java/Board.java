/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Natthakritta
 */
public class Board {

    private char[][] table = {{'-', '-', '-'},
                                                                                 {'-', '-', '-'},
                                                                                 {'-', '-', '-'}};
    private Player curentPlayer;
    private Player O;
    private Player X;
    private int count;
    public boolean win = false;
    public boolean draw = false;

    public Board(Player O, Player X) {
        this.O = O;
        this.X = X;
        this.curentPlayer = O;
        this.count =0;
    }

    public char[][] getTable() {
        return table;
    }

    public Player getCurentPlayer() {
        return curentPlayer;
    }

    public Player getO() {
        return O;
    }

    public Player getX() {
        return X;
    }
    
    private  void switchPlayer(){
        if (curentPlayer == O) {
            curentPlayer = X;
        }else{
            curentPlayer = O;
        }
    }
    
    public boolean setRowCol(int row, int col) {
        if (row > 3 || col > 3 || row < 1 || col < 1) {
            return false;
        }
        if (table[row - 1][col - 1] != '-') {
            return false;
        }
        table[row - 1][col - 1] = curentPlayer.getSymbol();
        if (checkWin(row, col)) {
            return UpdateScore();
        }
        if (checkDraw(count)) {
            O.Draw();
            X.Draw();
            this.draw = true;
            return true;
        }
        count++;
        switchPlayer();
        return true;
    }
    
    private boolean UpdateScore() {
        if (this.curentPlayer == O) {
            O.Win();
            X.Loss();
        }else{
            X.Win();
            O.Loss();
        }
        this.win = true;
        return true;
    }
    
    public boolean checkWin(int row, int col) {
        if (checkVertical(col)) {
            return true;
        } else if (checkHorizontal(row)) {
            return true;
        } else if (checkX()) {
            return true;
        }
        return false;
    }
    
      public boolean checkVertical(int col) {
        for (int r = 0; r < table.length; r++) {
            if (table[r][col - 1] != curentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkHorizontal(int row) {
        for (int c = 0; c < table.length; c++) {
            if (table[row - 1][c] != curentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkX() {
        if (checkXL()) {
            return true;
        } else if (checkXR()) {
            return true;
        }
        return false;
    }

    public boolean checkXL() {
        for (int i = 0; i < table.length; i++) {    //11 22 33
            if (table[i][i] != curentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkXR() {  //13 22 31
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != curentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }
    
    public static boolean checkDraw(int count) {
        if (count ==8) {
            return true;
        }
        return false;
    }

    public boolean isWin() {
        return win;
    }

    public boolean isDraw() {
        return draw;
    }
    
    }
